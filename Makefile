VERSION=v1-6
COMPONENT=base
TAG=$(COMPONENT):$(VERSION)

.MAIN: build

build:
	sudo docker build -t $(TAG) .

run:
	sudo docker run -e PGNAME=monuser -it $(TAG) bash

runc:
	sudo docker run --entrypoint="" -it $(TAG) bash

.PHONY: build
