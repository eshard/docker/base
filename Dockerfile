FROM debian:12
LABEL maintainer="Georges Gagnerot<georges.gagnerot@eshard.com>"

ENV  DEBIAN_FRONTEND=noninteractive
RUN  apt update && apt install -y openbox menu tigervnc-standalone-server tmux sudo xterm vim wget feh

# Set locale (fix locale warnings)
RUN  echo "Europe/Paris" > /etc/timezone

RUN useradd -ms /bin/bash user && usermod -aG sudo user
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN wget https://github.com/tianon/gosu/releases/download/1.17/gosu-i386 -O /bin/gosu && chmod +rx /bin/gosu

COPY ./launch /bin/launch
CMD  ["/bin/launch"]
COPY ./init-adduser /bin/init-adduser
ENTRYPOINT ["/bin/init-adduser"]

USER user
WORKDIR /home/user

#default password
ENV PASSWORD=esdynamic
#The cmdline we will launch
ENV AUTOSTART=""
#tigervnc MaxIdleTime
ENV VNC_MAX_IDLE_TIME=0
#tigervnc IdleTimeout
ENV VNC_IDLE_TIMEOUT=0
#open box configuration
COPY --chown=user:user .config /home/user/.config
COPY --chown=user:user .Xresources /home/user/.Xresources

#Labels
LABEL eshard.metaenv.registry.enabled="true"
LABEL eshard.metaenv.registry.type="vnc"
LABEL eshard.metaenv.registry.icon="FaDocker"
LABEL eshard.metaenv.registry.description="Base"
